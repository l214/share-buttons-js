# Share Buttons with Js

If client brower supports share API, this will display a generic share button that will call the sandard API share available on Androïd and iOS. 
If not, it will display share buttons with mains socials networks.

To keep architecture blazing simple, all is inside a single file js-share-l214.js, that contains HTML, CSS ans JS.

This file is hosted straightly on GitLab and served by CloudFlare.


## Minimal configuration CDN version

Will use title, description and canonical defined in the page :

```html
<div class="js-share-l214" ></div>
<script src="https://js-share-l214.power214.workers.dev"></script>
```


## Minimal configuration local version
```html
<div class="js-share-l214" ></div>
<script src="js-share-l214.js" defer></script>
```


## Custom configuration example
```html
<div class="js-share-l214" 
     data-url="https://www.chips.info"
     data-title="I <3 chips"
     data-tweet="J'aime les @chips"
     data-message="Je viens de tomber sur cet article de chips.info, il est impressionnant !"></div>
<script src="js-share-l214.js" defer></script>
```


## Try in seconds with inspector

* Open inspector on any page
* Add a <div class="js-share-l214"></div> bu editing like HTML any div of the DOM
* Go to the console and simply copy/paste the js-share.l214.js content or hit :
```javascript
document.head.appendChild(document.createElement('script')).src="https://js-share-l214.power214.workers.dev";
```

## Icon colors

You can let natural colors or add a css line to unify colors :

```css
.share-l214 a:not(:hover) {color:orange !important;}
```

You can see it in action in demo.html. Comment it to see natural colors :-)

## Display LinkedIn button

Add data attribute linkedin="1" :

```html
<div class="js-share-l214"
     data-linkedin="1"></div>
```

## Test demo

```bash
node server.js
```

and go to demo.html with your browser.

## Force the share api display even if the share api is not available (ex: on your desktop)

Simply add in the .js-share-l214 div `data-forceapishare="1"``

ex :
```html
<div class="js-share-l214"
     data-forceapishare="1"
     data-url="https://www.chips.info"
     data-title="I <3 chips"
     data-tweet="J'aime les @chips"
     data-message="Je viens de tomber sur cet article de chips.info, il est impressionnant !"></div>
<script src="js-share-l214.js" defer></script>
```
