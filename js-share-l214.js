// addCSS('js-share-l214.css')
//let shareWidgets = document.querySelectorAll(".js-share-l214")

/** HTML ***********/
let htmlTemplateShareAPI = `
<div class="share-l214-shareapi">
<button class="jsl-shareapi" type="button">
    <svg><use href="#share-icon"></use></svg>
    <span>PARTAGER</span>
</button>
<svg style="display: none;">
  <defs>
    <symbol id="share-icon" viewBox="-2 -4 32 32"   fill="currentColor"         stroke="currentColor" stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-share"><path d="M16.272 5.451c-.176-.45-.272-.939-.272-1.451 0-2.208 1.792-4 4-4s4 1.792 4 4-1.792 4-4 4c-1.339 0-2.525-.659-3.251-1.67l-7.131 3.751c.246.591.382 1.239.382 1.919 0 .681-.136 1.33-.384 1.922l7.131 3.751c.726-1.013 1.913-1.673 3.253-1.673 2.208 0 4 1.792 4 4s-1.792 4-4 4-4-1.792-4-4c0-.51.096-.999.27-1.447l-7.129-3.751c-.9 1.326-2.419 2.198-4.141 2.198-2.76 0-5-2.24-5-5s2.24-5 5-5c1.723 0 3.243.873 4.143 2.201l7.129-3.75zm3.728 11.549c1.656 0 3 1.344 3 3s-1.344 3-3 3-3-1.344-3-3 1.344-3 3-3zm-15-9c2.208 0 4 1.792 4 4s-1.792 4-4 4-4-1.792-4-4 1.792-4 4-4zm15-7c1.656 0 3 1.344 3 3s-1.344 3-3 3-3-1.344-3-3 1.344-3 3-3z"/></symbol>
  </defs>
</svg>
`

let htmlTemplateFallBack = `
<div class="share-l214">
    <a class="jsl-facebook" href="https://www.facebook.com/sharer.php?u=--urlEncoded--" target="_blank">
      <svg><use href="#facebook"></use></svg>
      <span>Facebook</span>
    </a>
    <a class="jsl-twitter" href="https://twitter.com/intent/tweet?text=--tweetEncoded--" target="_blank">
      <svg><use href="#twitter"></use></svg>
      <span>Twitter</span>
    </a>
    <a class="jsl-whatsapp" href="https://api.whatsapp.com/send?text=--messageEncoded--" target="_blank">
      <svg><use href="#whatsapp"></use></svg>
      <span>WhatsApp</span>
    </a>
    <a class="jsl-gmail" href="https://mail.google.com/mail/u/0/?ui=2&amp;view=cm&amp;fs=1&amp;tf=1&amp;su=--titleEncoded--&amp;body=--messageEncoded--" target="_blank">
      <svg><use href="#gmail"></use></svg>
      <span>Gmail</span>
    </a>
    <a class="jsl-email" href="mailto:?subject=--titleEncoded--&body=--messageEncoded--" target="_blank">
      <svg><use href="#email"></use></svg>
      <span>Email</span>
    </a>
    <a class="jsl-linkedin" href="https://www.linkedin.com/shareArticle?mini=true&amp;url=--urlEncoded--&amp;title=--titleEncoded--&amp;summary=--messageEncoded--" target="_blank">
      <svg><use href="#linkedin"></use></svg>
      <span>LinkedIn</span>
    </a>
    <a class="jsl-copylink" href="#" onClick="return jslCopyToClipboard(event, '--url--'); return false;">
      <svg><use href="#copylink"></use></svg>
      <span>Copier le lien</span>
    </a>
</div>

<svg style="display: none;">
  <defs>
    <symbol id="facebook"   viewBox="-2 -2 29 29"   fill="currentColor" stroke="currentColor"      stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-facebook"><path d="M18 2h-3a5 5 0 0 0-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 0 1 1-1h3z"></path></symbol>
    <symbol id="twitter"    viewBox="-8 -8 38 38" fill="currentColor" stroke="currentColor"      stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-twitter"><path d="M14.1 10.3 22.3 1h-2l-7 8-5.7-8H1l8.6 12.2L1 23h2l7.5-8.5 6 8.5H23l-8.9-12.7zm-2.7 3-.8-1.2-7-9.7h3l5.6 7.9.9 1.2 7.2 10.1h-3l-5.9-8.3z"/></symbol>
    <symbol id="email"      viewBox="-2 -2 28 28"   fill="currentColor" stroke="#FFF"      stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></symbol>
    <symbol id="gmail"      viewBox="-5 -5 40 40"   fill="currentColor"> <path d="M3.3 4.2A2.704 2.704 0 00.6 6.9v.255L15 17.4 29.4 7.155V6.9c0-1.488-1.212-2.7-2.7-2.7H3.3zm.51 1.2h22.377L15 13.2 3.81 5.4zM.6 8.416V23.1c0 1.488 1.212 2.7 2.7 2.7h23.4c1.488 0 2.7-1.212 2.7-2.7V8.416l-3.6 2.562V24.6H4.2V10.978L.6 8.416z" fill="currentColor"></path></symbol>
    <symbol id="linkedin"   viewBox="-4 -4 32 32" fill="currentColor" stroke="currentColor"      stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-linkedin"><path d="M16 8a6 6 0 0 1 6 6v7h-4v-7a2 2 0 0 0-2-2 2 2 0 0 0-2 2v7h-4v-7a6 6 0 0 1 6-6z"></path><rect x="2" y="9" width="4" height="12"></rect><circle cx="4" cy="4" r="2"></circle></symbol>
    <symbol id="whatsapp"   viewBox="0 0 24 24"   fill="currentColor" stroke="#FFF"         stroke-width="1" stroke-linecap="round" stroke-linejoin="round" class="feather feather-whatsapp"><path d="M12.031 6.172c-3.181 0-5.767 2.586-5.768 5.766-.001 1.298.38 2.27 1.019 3.287l-.582 2.128 2.182-.573c.978.58 1.911.928 3.145.929 3.178 0 5.767-2.587 5.768-5.766.001-3.187-2.575-5.77-5.764-5.771zm3.392 8.244c-.144.405-.837.774-1.17.824-.299.045-.677.063-1.092-.069-.252-.08-.575-.187-.988-.365-1.739-.751-2.874-2.502-2.961-2.617-.087-.116-.708-.94-.708-1.793s.448-1.273.607-1.446c.159-.173.346-.217.462-.217l.332.006c.106.005.249-.04.39.298.144.347.491 1.2.534 1.287.043.087.072.188.014.304-.058.116-.087.188-.173.289l-.26.304c-.087.086-.177.18-.076.354.101.174.449.741.964 1.201.662.591 1.221.774 1.394.86s.274.072.376-.043c.101-.116.433-.506.549-.68.116-.173.231-.145.39-.087s1.011.477 1.184.564.289.13.332.202c.045.072.045.419-.1.824zm-3.423-14.416c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm.029 18.88c-1.161 0-2.305-.292-3.318-.844l-3.677.964.984-3.595c-.607-1.052-.927-2.246-.926-3.468.001-3.825 3.113-6.937 6.937-6.937 1.856.001 3.598.723 4.907 2.034 1.31 1.311 2.031 3.054 2.03 4.908-.001 3.825-3.113 6.938-6.937 6.938z"/></symbol>
    <symbol id="copylink"   viewBox="-6 -6 35 35" fill="currentColor" stroke="#FFF"         stroke-width=".2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-copylink"><path d="M6.188 8.719c.439-.439.926-.801 1.444-1.087 2.887-1.591 6.589-.745 8.445 2.069l-2.246 2.245c-.644-1.469-2.243-2.305-3.834-1.949-.599.134-1.168.433-1.633.898l-4.304 4.306c-1.307 1.307-1.307 3.433 0 4.74 1.307 1.307 3.433 1.307 4.74 0l1.327-1.327c1.207.479 2.501.67 3.779.575l-2.929 2.929c-2.511 2.511-6.582 2.511-9.093 0s-2.511-6.582 0-9.093l4.304-4.306zm6.836-6.836l-2.929 2.929c1.277-.096 2.572.096 3.779.574l1.326-1.326c1.307-1.307 3.433-1.307 4.74 0 1.307 1.307 1.307 3.433 0 4.74l-4.305 4.305c-1.311 1.311-3.44 1.3-4.74 0-.303-.303-.564-.68-.727-1.051l-2.246 2.245c.236.358.481.667.796.982.812.812 1.846 1.417 3.036 1.704 1.542.371 3.194.166 4.613-.617.518-.286 1.005-.648 1.444-1.087l4.304-4.305c2.512-2.511 2.512-6.582.001-9.093-2.511-2.51-6.581-2.51-9.092 0z"/></symbol>
  </defs>
</svg>`
/** HTML:END ***********/

/** CSS ****************/
let widgetCSS = `
.share-l214 {
  display: inline-flex;
  position: relative;
  box-sizing: border-box;
}
.share-l214 a {
  display: inline-flex;
  width: 50px;
  height: 50px;
  margin: 4px 5px;
}
.share-l214 a.jsl-facebook {color: #3b5998 ;}
.share-l214 a.jsl-twitter {color: #000;}
.share-l214 a.jsl-email {color: #777;}
.share-l214 a.jsl-gmail {color: #E22;}
.share-l214 a.jsl-linkedin {color: #0077B5;}
.share-l214 a.jsl-whatsapp {color: #1bd741;}
.share-l214 a.jsl-copylink {color: #555;}
.share-l214 a span {display: none;}
.share-l214 a svg {
  width: 100%;
  height: auto;
  /* margin-right: 4px; */
  background: white;
  border-radius: 100px;
  margin:2px; 
  padding: 2px;
  box-sizing: content-box;
  box-shadow: 1px 0px 2px rgba(0, 0, 0, 0.10), 1px 1px 7px rgba(0, 0, 0, 0.2); 
  transition: all .2s ease-out;
}
.share-l214 a:hover svg {
  transform: scale(.95);
  box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.12);
}
.share-l214 a:hover span {
  position: absolute;
  left:0;
  top:110%;
  width:100%;
  display:block;
  color: #444;
  text-align:center;
  white-space: nowrap;
}
.share-l214 .jsl-label { text-align:center; }
@media all and (min-width: 940px) {
  .jsl-whatsapp {display: none !important;}
}

.share-l214 a.jsl-copylink:focus svg {
    animation: copyanim .8s;
}
@keyframes copyanim {
  0%   { background-color: orange; }
  100% { background-color: white;  }
}

/* SHARE API CSS */
.share-l214-shareapi button.jsl-shareapi {
    display:block;
    background:none;
    color:orange;
    text-decoration:none;
    border: none;
    outline: none;
}
.share-l214-shareapi button.jsl-shareapi svg {
    display:inline-block;
    width: 50px;
    height: 50px;
    background: #FFF;
    border-radius:50px;
    padding:10px;
    border:orange solid 3px;
    box-shadow: 1px 3px 7px #ddd, 1px 1px 3px #aaa;
    vertical-align: middle;

}
.share-l214-shareapi button.jsl-shareapi span {
    display:inline-block;
    color: #000;
    padding-left:5px;
}

.share-l214-shareapi button.jsl-shareapi:hover svg {
    animation: focusanim 8s;
}
@keyframes focusanim {
  0%   { background-color: white;  color: orange; }
  25%  { background-color: orange; color: white;  }
  50%  { background-color: white;  color: orange; }
  75%  { background-color: orange; color: white;  }
  100% { background-color: white;  color: orange; }
}


`
/** CSS:END ************/

function shareL214() {
  let isLinkedin = false;
  let isCopyButton = false;

  let shareWidgets = document.querySelectorAll(".js-share-l214")
  shareWidgets.forEach(function(widget) {
      const metaDescription = document.querySelector("meta[name='description']");
      let url   = widget.dataset.url || document.querySelector("link[rel='canonical']").getAttribute("href");
      let title = widget.dataset.title || document.title;
      let message  = widget.dataset.message || (metaDescription ? metaDescription.getAttribute('content') : url);
      let tweet = widget.dataset.tweet || url;

      // For now, Web Share API is disabled because of issues on Windows and Chrome OS
      const shareApiSupport = false || widget.dataset.forceapishare; //typeof navigator.share !== "undefined";

      let widgetCode = shareApiSupport ? htmlTemplateShareAPI : htmlTemplateFallBack;
      isLinkedin = widget.dataset.linkedin ? (widget.dataset.linkedin === "1") : false;
      isCopyButton = (navigator.clipboard) ? true : false;

      widget.innerHTML = widgetCode
      .replace(/--url--/g, url)
      .replace(/--urlEncoded--/g, encodeURIComponent(url))
      .replace(/--titleEncoded--/g, encodeURIComponent(title))
      .replace(/--tweetEncoded--/g, encodeURIComponent(tweet))
      .replace(/--messageEncoded--/g, encodeURIComponent(message))
      
      if(shareApiSupport) { 
          widget.addEventListener('click', function(event) {
              event.stopPropagation();
              navigator.share({"title": title, "url": url, "text": message});
              return false;
          })
      }
  })

  if(!isLinkedin) {
      widgetCSS += `.share-l214 a.jsl-linkedin {display:none;}`;
  }
  if(!isCopyButton) {
      widgetCSS += `.share-l214 a.jsl-copylink {display:none;}`;
  }
  jslAddCss(widgetCSS)
}
shareL214();

/* *********************** */
/* ** HELPERS ************ */
/* *********************** */
function jslAddCss(cssCode)
{
var styleElement = document.createElement("style");
  styleElement.type = "text/css";
  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = cssCode;
  } else {
    styleElement.appendChild(document.createTextNode(cssCode));
  }
  document.getElementsByTagName("head")[0].appendChild(styleElement);
}


async function jslCopyToClipboard(event, text)
{
  /* To prevent url change */
  event = event || window.event;
  event.preventDefault();

  try {
    await navigator.clipboard.writeText(text);
  } catch (err) {
    console.error('Échec de la copie', err);
  }
  return false;
}
